const images = {};
images.skeleton = new Image();
images.skeleton.src = 'image/swordskeleton.png'
const skeletonActions = ['walk', 'walkandattackfromleft', 'walkdown'];
const enemies = [];
const numberOfEnemies = 100;
class Skeleton {
  constructor() {
    this.width = 128;
    this.height = 128;
    this.frameX = 0;
    this.frameY = 0;
    this.x = 0;
    this.y = 0;
    this.speed = 2;
    this.action = skeletonActions[1];

    if (this.action === 'walk') {
      this.frameY = 0;
      this.minFrame = 0;
      this.maxFrame = 19;
    }
    if (this.action === 'walkandattackfromleft') {
      this.frameY = 7;
      this.minFrame = 0;
      this.maxFrame = 32;
    }
    if (this.action === 'walkdown') {
      this.frameY = 0;
      this.minFrame = 0;
      this.maxFrame = 19;
    }
  }
  draw(ctx) {
    ctx.drawImage(images.skeleton,
      this.width * this.frameX,
      this.height * this.frameY,
      this.width, this.height,
      this.x,
      this.y,
      this.width,
      this.height);

    if (this.frameX < this.maxFrame) {
      this.frameX++;
    }
    else this.frameX = this.minFrame;

  }

  update() {
    if (this.action === 'walk') {
      if (this.x > canvas.width + this.width) {

        this.x = 0 - this.height;
        this.y = 2;
      }
      else {

        this.x += this.speed;
      }
    }
    if (this.action === 'walkandattackfromleft') {
      if (this.x < 0) {
        this.x = canvas.width;
        this.y = 2;
      }
      else {
        this.x -= this.speed;
      }
    }
    if (this.action === 'walkdown') {
      if (this.y < 0) {
        this.y = canvas.height;
        this.x = 2;
      }
      else {
        this.y -= this.speed;
      }
    }
    //console.log(this.y);






  }
}


const hulk = {
  x: 0,
  y: 0,
  width: 40,
  height: 56,
  frameX: 0,
  frameY: 0,
  speed: 9,
  moving: false,
};
const orc = {
  x: 0,
  y: 0,
  width: 380,
  height: 349.473684,
  frameX: 0,
  frameY: 0,
  speed: 2,
};

const skeleton = {
  x: 0,
  y: 0,
  width: 128,
  height: 128,
  frameX: 0,
  frameY: 0,
  speed: 2,

}


const hulkSprite = new Image();
hulkSprite.src = 'image/hulk.png';
const orcSprite = new Image();
orcSprite.src = 'image/orc.png';

const skeletonSprite = new Image();
skeletonSprite.src = 'image/swordskeleton.png';

function draw(gameState, canvas) {
  let ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  grid(ctx, maxX, maxY);
  roadDraw(ctx, gameState.roadList);
  towerdraw(ctx, gameState.towerList, gameState.selectedTower);

  enemyDraw(ctx, gameState.enemyList, gameState.lastUpdate);
  //animateEnemy(ctx, gameState.enemyList);

  bulletDraw(ctx, gameState.bulletList);

  document.getElementById('lives').innerHTML =
    'Lives: ' + gameState.livesRemaining;
  let money = document.getElementById('money');
  if (money) {
    money.innerHTML = 'Money: ' + gameState.money;
  }
  let score = document.getElementById('score');
  if (score) {
    score.innerHTML = 'Score: ' + gameState.score;
  }
  document.getElementById('gamespeed').innerHTML = 'gamespeed: ' + gameSpeed;
}

function grid(ctx, maxX, maxY) {
  ctx.lineWidth = 1;
  for (let x = 0; x < maxX; x++) {
    for (let y = 0; y < maxY; y++) {
      ctx.strokeStyle = '#ffffff';
      ctx.strokeRect(
        cellSize * x + 0.5,
        cellSize * y + 0.5,
        cellSize,
        cellSize
      );
    }
  }
}
/* let tID;
function animateScript() {
  let position = 175; // start position for the image slicer
  const interval = 100; // 100ms of interval for the setInterval()

  tID = setInterval(() => {
    document.getElementById(
      'image'
    ).style.backgroundPosition = `-${position}px 0px`;

    if (position < 1050) {
      position += 175;
    } else {
      position = 175;
    }
  }, interval);
} */

function towerdraw(ctx, towerList, selectedTower) {
  ctx.save();
  for (let i = 0; i < towerList.length; i++) {
    let towerType = towerList[i].towerId.id;
    let img;
    switch (towerType) {
      case 'arrowTower':
        img = document.getElementById('tower');
        break;
      case 'cannon':
        img = document.getElementById('turret');
        break;
      case 'icetower':
        img = document.getElementById('ice');
        break;
      case 'flametower':
        img = document.getElementById('flame');
        break;
    }
    /*     ctx.beginPath();
    ctx.ellipse(
      towerList[i].gridX * cellSize + cellSize / 2,
      towerList[i].gridY * cellSize + cellSize / 2,
      cellSize / 2,
      cellSize / 2,
      0,
      0,
      2 * Math.PI
    );
    ctx.fill();
    ctx.closePath(); */

    ctx.drawImage(
      img,
      towerList[i].gridX * cellSize,
      towerList[i].gridY * cellSize,
      img.height / (cellSize / 2),
      img.width / (cellSize / 2)
    );
    // alaptorony kiválasztás kikapcsolása
    if (selectedTower === towerList[i]) {
      towerRangeDraw(ctx, towerList[i]);
    }

  }
  ctx.restore();
}

function towerRangeDraw(ctx, tower) {
  ctx.strokeStyle = 'RED';
  ctx.globalAlpha = 1;
  ctx.beginPath();
  ctx.ellipse(
    tower.gridX * cellSize + cellSize / 2,
    tower.gridY * cellSize + cellSize / 2,
    tower.range * cellSize,
    tower.range * cellSize,
    0,
    0,
    2 * Math.PI
  );
  ctx.stroke();
  ctx.globalAlpha = 1;
  ctx.closePath();
}

function roadDraw(ctx, roadList) {
  ctx.fillStyle = 'LIGHTGREY';
  for (let i = 0; i < roadList.length; i++) {
    ctx.beginPath();
    ctx.fillRect(
      roadList[i].gridX * cellSize,
      roadList[i].gridY * cellSize,
      cellSize,
      cellSize
    );
    ctx.closePath();
  }
}

function drawLife(ctx, x, y, life, maxLife) {
  ctx.save();
  ctx.strokeRect(x - 5, y - 10, 10, 2);
  ctx.fillStyle = 'GREEN';
  ctx.fillRect(x - 5, y - 10, (10 * life) / maxLife, 2);
  ctx.restore();
}

let srcX;
let srcY;

/* function animateEnemy(ctx, enemyList) {
  let character = new Image();
  character.src = 'image/character.png';
  let sheetWidth = character.width;
  let sheetHeight = character.height;
  let cols = 8;
  let rows = 2;
  let width = sheetWidth / cols;
  let height = sheetHeight / rows;
  let currentFrame = 0;

  let trackLeft = 1;
  let trackRight = 0;

  for (let i = 0; i < enemyList.length; ++i) {
    setInterval(updateFrame(currentFrame, enemyList[i].x, enemyList[i].y), 10);

    ctx.drawImage(
      character,
      srcX,
      srcY,
      width,
      height,
      enemyList[i].x,
      enemyList[i].y,
      width * 2 / (cellSize / 2.5),
      height * 2 / (cellSize / 2.5)
    );
    drawLife(
      ctx,
      enemyList[i].x,
      enemyList[i].y,
      enemyList[i].life,
      enemyList[i].maxLife
    );
  }
  function updateFrame(currentFrame, x, y) {
    let character = new Image();
    character.src = 'image/character.png';
    let sheetWidth = character.width;
    let sheetHeight = character.height;
    let cols = 8;
    let rows = 2;
    let width = sheetWidth / cols;
    let height = sheetHeight / rows;
    let trackLeft = 1;
    let trackRight = 0;
    //ctx.clearRect(x,y, width, height);

    currentFrame = ++currentFrame % cols;

    console.log(currentFrame);

    srcX = currentFrame * width;
    if (x > 0) {
      x -= 5;
      srcY = trackLeft * height;
    } else {
      if (x > canvas.width - width) {
        x += 5;
        y = trackRight * height;
      }
    }
  }
} */




class Orc {
  constructor() {
    this.width = 380;
    this.height = 349.473684;
    this.frameX = 0;
    this.frameY = 0;
    //this.elapsed = elapsed;
    //this.counter = counter;
    this.images = {};
    this.images.orc = new Image();
    this.images.orc.src = 'orc.png';
    this.orcActions = ['walk', 'walkleft', 'walkdown'];
    this.action = this.orcActions[0];

    /* 
        if (this.now % 100 === 0) {
          if (this.counter < 10) { */

    /* else {
      this.counter = 0;
    } */
    /* console.log('if', this.counter);
  }
  else this.counter = 0;
}
console.log('end', this.counter);
*/





    let actCount = counter;

    if (this.action === 'walk') {
      this.frameY = 0;
      this.minFrame = 0;
      this.maxFrame = 19;
    }
    this.frameX = counter + this.frameX;



    /*  let c = Math.floor(counter / 10);
     console.log('c' ,c); */
  }


  draw(ctx, x, y, lastUpdate) {
    ctx.drawImage(this.images.orc,
      this.width * this.frameX,
      this.height * this.frameY,
      this.width, this.height,
      x,
      y,
      this.width,
      this.height);


    if (lastUpdate % 1 === 0) {
      if (this.frameX < this.maxFrame) {
        ++this.frameX;
        console.log('fx:', this.frameX, ' counter: ', counter);
      }
      else {
        this.frameX = 0;
        counter = 0;
      }
    }
    console.log('fx:', this.frameX, ' counter: ', counter);



    //console.log(lastUpdate);
    /*     console.log('elapsed' + this.elapsed); */
    /*  if (lastUpdate % 10 === 0) {
       ++this.counter;
       console.log(this.counter);
     } */
    //console.log('undi', this.elapsed);
    //console.log('lu:' + lastUpdate);

    //this.elapsed = (this.now - lastUpdate);


    /* 
        if (this.action === 'walk' && this.elapsed < 15) {
    
          if (this.frameX <= this.maxFrame) {
    
            this.frameX++;
    
          }
          else {
    
            this.frameX = this.minFrame;
    
          }
    
    
    
        }
     */
  }


}


let counter = 0;

let fps, fpsInterval, startTime, now, then, elapsed;
function counterFps(lastUpdate) {
  now = lastUpdate;
  elapsed = now - then;
  if (elapsed > fpsInterval) {
    then = now - (elapsed % fpsInterval);
  }
  ++counter;

  requestAnimationFrame(frame);
}

function startCounter(fps) {
  fpsInterval = 1000 / fps;
  then = Date.now();
  startTime = then;

  ++counter;


  counterFps(now);

}

startCounter(10);


function enemyDraw(ctx, enemyList, lastUpdate) {



  /*   if (lastUpdate % 3 === 0) {
      if (counter < 19) {
        counter++; */
  //console.log('counter < 10', counter);
  /*     }
      else counter = 0;
    } */
  //console.log('end', counter);


  let img;
  let then;


  //let elapsed = 0; //gamestate-be akár
  let orc = new Orc();
  function drawSprite(img, sX, sY, sW, sH, dX, dY, dW, dH) {
    ctx.drawImage(img, sX, sY, sW, sH, dX, dY, dW, dH)
  }



  /*   } */
  //console.log(elapsed);


  for (let i = 0; i < enemyList.length; i++) {
    let enemyType = enemyList[i].id;
    //console.log(enemyType);

    switch (enemyType) {
      default:
        /* ctx.drawImage(
          orcSprite,
          elapsed * orc.width * timer_is_on,
          0,
          orc.width,
          orc.height,
          enemyList[i].x - cellSize,
          enemyList[i].y - cellSize,
          orc.height / (cellSize / 4),
          orc.width / (cellSize / 4)
        ); */

        orc.draw(ctx, enemyList[i].x, enemyList[i].y, lastUpdate, 0);
        break;
    }
    /*      case 'hulk':
      ctx.drawImage(
      hulkSprite,
      0 + elapsed * hulk.width * timer_is_on,
      0,
      hulk.width,
      hulk.height,
      enemyList[i].x - cellSize,
      enemyList[i].y - cellSize,
      hulk.height,
      hulk.width
      break;
    case 'orc':
      ctx.drawImage(
        orcSprite,
        0 + elapsed * orc.width * timer_is_on,
        0,
        orc.width,
        orc.height,
        enemyList[i].x - cellSize,
        enemyList[i].y - cellSize,
        orc.height / (cellSize / 4),
        orc.width / (cellSize / 4)
      );
      break;
    case 'skeleton':


      ctx.drawImage(
        skeletonSprite,
        0 + frame,
        0 + frame,
        skeleton.width,
        skeleton.height,
        enemyList[i].x,
        enemyList[i].y,
        skeleton.height,
        skeleton.width
      );
      break; */

    /*       
        } */
    //img = document.getElementById('Character'); //, cellSize, cellSize);
    /* for (let j = 0; j < 4; ++j) {
      //console.log(j);
      ctx.drawImage(
        orcSprite,
        j,
        j,
        orc.width,
        orc.height,
        enemyList[i].x,
        enemyList[i].y,
        orc.height / (cellSize / 2),
        orc.width / (cellSize / 2)
      );
    } */ /*  ctx.beginPath();
    ctx.fillStyle = 'RED';
    ctx.ellipse(
      enemyList[i].x,
      enemyList[i].y,
      enemyList[i].size / 2,
      enemyList[i].size / 2,
      0,
      0,
      2 * Math.PI
    );
    ctx.fill();
    ctx.closePath(); */
    /* let img = document.getElementById('enemy');
      ctx.drawImage(
        img,
        enemyList[i].x - cellSize / 2,
        enemyList[i].y + cellSize,
        img.height / (cellSize / 2),
        img.width / (cellSize / 2)
        );  */
    drawLife(
      ctx,
      enemyList[i].x,
      enemyList[i].y,
      enemyList[i].life,
      enemyList[i].maxLife
    );
  }
  //animate();
}

function enemy2Draw(ctx, enemyList, enemy) {
  let img;

  for (let i = 0; i < enemyList.length; i++) {
    img = document.getElementById('Character'); //, cellSize, cellSize);
    for (let j = 0; j < 4; ++j) {
      //console.log(j);
      ctx.drawImage(
        orcSprite,
        0,
        0,
        orc.width * j,
        orc.height * j,
        enemyList[i].x,
        enemyList[i].y,
        orc.height / (cellSize / 2),
        orc.width / (cellSize / 2)
      );
    } /*  ctx.beginPath();
    ctx.fillStyle = 'RED';
    ctx.ellipse(
      enemyList[i].x,
      enemyList[i].y,
      enemyList[i].size / 2,
      enemyList[i].size / 2,
      0,
      0,
      2 * Math.PI
    );
    ctx.fill();
    ctx.closePath(); */
    /* let img = document.getElementById('enemy');
      ctx.drawImage(
        img,
        enemyList[i].x - cellSize / 2,
        enemyList[i].y + cellSize,
        img.height / (cellSize / 2),
        img.width / (cellSize / 2)
        );  */
    drawLife(
      ctx,
      enemyList[i].x,
      enemyList[i].y,
      enemyList[i].life,
      enemyList[i].maxLife
    );
  }
  //animate();
}

function bulletDraw(ctx, bulletList) {
  for (let i = 0; i < bulletList.length; i++) {
    ctx.beginPath();
    ctx.fillStyle = 'PINK';
    ctx.ellipse(
      bulletList[i].x,
      bulletList[i].y,
      bulletList[i].size / 2,
      bulletList[i].size / 2,
      0,
      0,
      2 * Math.PI
    );
    ctx.fill();
    ctx.closePath();
  }
}
/*
let skel = new Skeleton();

let fps, fpsInterval, startTime, now, then, elapsed;
function animate(ctx) {
  now = Date.now();
  elapsed = now - then;
  if (elapsed > fpsInterval) {
    then = now - (elapsed % fpsInterval);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    skel.draw();
    skel.update();

  }
  requestAnimationFrame(animate);
}

function startAnimating(ctx, fps) {
  fpsInterval = 1000 / fps;
  then = Date.now();
  startTime = then;
  animate(ctx);
}
startAnimating(20);

 */