const maxX = 44;
const maxY = 25;
const cellSize = 25;


/*TODO:

demolish,  animate enemy,   upgrade
  

   lövedék típus,lassító lövedék 
  
  enemies:
   (boss), 
   

  grid:   akadály, 

  user interface: 
  wave counter,

   game options: 
   set startpoint, set endpoint
  gameoverScreen; pályaméretezés (n,m: 10-25),sebesség (global)

  gameover with jason head;
 
  map editor, registration, save, 

  út


DONE:
global speed, pause, többféle enemy.id
 user interface (torony választó, pause, mentés, kilépés, élet, 
  pontszám, torony ár, hátralévő tornyok száma, hátralévő hullámok)
  ,pause,

towertypes, towerrange-visible,towerupgrade,
tornyok: pricefortowers; , enemy life + lifebars,
  towerrange, 
  enemietypes
  a* , ne lehessen tornyot rakni ha nincs út az enemy-nek
  ne lehessen tornyot rakni, ahol van enemy
  endpoint, enemydissaper, gamerlife-- bulletundraw done,

*/


function setup(editor) {
  let gameState = {
    livesRemaining: 10,
    money: 1000,
    score: 0,
    startPoint: { gridX: 0, gridY: 0 },
    endPoint: { gridX: maxX - 1, gridY: maxY - 1 },
    selectedTower: null,
    towerList: [
      /* {
        gridX: 11,
        gridY: 10,
        range: 1,
        reloadingTime: 2000,
        timeToReload: 2000,
      },
      {
        gridX: 9,
        gridY: 10,
        range: 2,
        reloadingTime: 2000,
        timeToReload: 2000,
      },
      {
        gridX: 10,
        gridY: 10,
        range: 8,
        reloadingTime: 2000,
        timeToReload: 2000,
      }, */
    ],
    enemyList: [],
    bulletList: [],
    roadList: [
      { gridX: 0, gridY: 0 },
      { gridX: 0, gridY: 1 },
      { gridX: 1, gridY: 1 },
      { gridX: 2, gridY: 1 },
      { gridX: 3, gridY: 1 },
      { gridX: 3, gridY: 2 },
      { gridX: 3, gridY: 3 },
      { gridX: 3, gridY: 4 },
      { gridX: 3, gridY: 5 },
      { gridX: 3, gridY: 6 },
      { gridX: 2, gridY: 7 },
      { gridX: 1, gridY: 8 },
      { gridX: 1, gridY: 9 },
      { gridX: 2, gridY: 10 },
      { gridX: 3, gridY: 11 },
      { gridX: 3, gridY: 12 },
      { gridX: 3, gridY: 13 },
      { gridX: 3, gridY: 14 },
      { gridX: 3, gridY: 15 },
      { gridX: 3, gridY: 16 },
      { gridX: 4, gridY: 16 },
      { gridX: 5, gridY: 16 },
      { gridX: 6, gridY: 16 },
      { gridX: 7, gridY: 16 },
      { gridX: 8, gridY: 16 },
      { gridX: 9, gridY: 16 },
    ],
    lastUpdate: Date.now(),
    gameStartTime: Date.now(),
    paused: false,
    gameTime: 0,
    waves: [
      {
        beginTime: 5,
        enemiesCounter: 1,
        enemy: { id: 'skeleton', life: 1, speed: 0.05, size: cellSize / 2 },

        repeateTime: 1000,
        lastDeployTime: 0,
      },
      {
        beginTime: 500,
        enemiesCounter: 0,
        enemy: { id: 'hulk', life: 3, speed: 0.15, size: cellSize / 3 },

        repeateTime: 1000,
        lastDeployTime: 0,
      },
      {
        beginTime: 5000,
        enemiesCounter: 0,
        enemy: { id: 'orc', life: 10, speed: 0.02, size: cellSize / 4 },

        repeateTime: 1000,
        lastDeployTime: 0,
      },
    ],
  };
  gameState.waves.forEach((wave) => {
    wave.enemy.maxLife = wave.enemy.life;
  });

  document.getElementById('save').addEventListener('click', function () {
    if (!editor) {
      localStorage.setItem('saveGame', JSON.stringify(gameState));
    }
    if (editor) {
      localStorage.setItem('saveEditor', JSON.stringify(gameState));
    }
  });

  document.getElementById('load').addEventListener('click', function () {
    let loadedGameState;
    if (!editor) {
      loadedGameState = localStorage.getItem('saveGame');
    } else {
      loadedGameState = localStorage.getItem('saveEditor');
    }
    Object.assign(gameState, JSON.parse(loadedGameState));
    //console.log(JSON.stringify(gameState));
  });
  document.getElementById('arrowTower').addEventListener('click', function () {
    gameState.tower = {
      towerId: arrowTower,
      range: 3,
      reloadingTime: 100,
      timeToReload: 0,
    };
    console.log(gameState.tower);
  });
  document.getElementById('cannon').addEventListener('click', function () {
    gameState.tower = {
      towerId: cannon,
      range: 8,
      reloadingTime: 2000,
      timeToReload: 0,
    };
  });
  document.getElementById('icetower').addEventListener('click', function () {
    gameState.tower = {
      towerId: icetower,
      range: 4,
      reloadingTime: 2000,
      timeToReload: 0,
    };
  });
  document.getElementById('flametower').addEventListener('click', function () {
    gameState.tower = {
      towerId: flametower,
      range: 2,
      reloadingTime: 2000,
      timeToReload: 0,
    };
  });

  const canvas = document.getElementById('myCanvas');
  canvas.width = window.innerWidth * 0.9;
  canvas.height = window.innerHeight * 0.9;
  setTimeout(() => frame(gameState, canvas, editor), 100);
  counterFps(gameState.lastUpdate);
  /*   frame(gameState, canvas, editor) */

  canvas.addEventListener(
    'contextmenu',
    function (click) {
      let newEnemy = makeEnemy(
        click.offsetX,
        click.offsetY,
        0.5,
        cellSize / 2,
        (Math.PI * 3) / 4
      );
      makeEnemyPath(
        newEnemy,
        gameState.towerList,
        gameState.roadList,
        gameState.endPoint
      );
      calculateEnemyDirection(newEnemy);
      gameState.enemyList.push(newEnemy);
      click.preventDefault();
      return false;
    },
    false
  );

  //itt rakhatnám be a törlést, és kapcsolhatnám ki azt a default tornyot az elején.
  canvas.addEventListener(
    'click',
    function (click) {
      let cellcoord = coordsToGrid(click.offsetX, click.offsetY);
      placeTower(gameState, cellcoord);
      gameState.towerList.forEach((tower) => {
        if (tower.gridX == cellcoord.gridX && tower.gridY == cellcoord.gridY) {
          gameState.selectedTower = tower;
        }
      });
    },
    false
  );

  if (!editor) {
    gameState.name = 'Gamer'; //= prompt("Enter Your Name: ");
  }
} // end of setup

function enemiesStillOnStage(endPoint, enemyList) {
  let endPointCoords = gridToCoordsCenter(endPoint.gridX, endPoint.gridY);
  return enemyList.filter(
    (enemy) =>
      !(
        Math.abs(enemy.x - endPointCoords.x) < enemy.size &&
        Math.abs(enemy.y - endPointCoords.y) < enemy.size
      )
  );
}

function update(gameState, canvas, elapsedTime) {
  gameState.gameTime += elapsedTime;
  moveEnemy(gameState.enemyList, elapsedTime);
  moveBullet(gameState.bulletList, elapsedTime);
  let { survivorList, remainingBulletList } = checkShoot(
    gameState.enemyList,
    gameState.bulletList
  );
  let kills = gameState.enemyList.length - survivorList.length;
  gameState.money += kills * 10;
  gameState.score += kills * 99;
  gameState.enemyList = survivorList;
  gameState.bulletList = remainingBulletList;
  bulletShoot(
    gameState.towerList,
    gameState.enemyList,
    elapsedTime,
    gameState.bulletList
  );
  createWave(
    gameState.waves,
    gameState.gameTime,
    gameState.enemyList,
    gameState.towerList,
    gameState.roadList,
    gameState.startPoint,
    gameState.endPoint
  );
  gameState.bulletList = visibleBullets(gameState.bulletList, canvas);
  let playingEnemies = enemiesStillOnStage(
    gameState.endPoint,
    gameState.enemyList
  );
  let livesLost = gameState.enemyList.length - playingEnemies.length;
  gameState.enemyList = playingEnemies;
  gameState.livesRemaining -= livesLost;
}

function visibleBullets(bulletList, canvas) {
  return bulletList.filter(
    (bullet) =>
      bullet.x > -bullet.size &&
      bullet.y > -bullet.size &&
      bullet.x < canvas.width + bullet.size &&
      bullet.y < canvas.height + bullet.size
  );
}

function createWave(
  waves,
  gameTime,
  enemyList,
  towerList,
  roadList,
  startPoint,
  endPoint
) {
  waves.forEach((wave) => {
    if (
      gameTime > wave.beginTime &&
      gameTime < wave.beginTime + wave.enemiesCounter * wave.repeateTime
    ) {
      if (wave.lastDeployTime < gameTime - wave.repeateTime) {
        let startCoordinate = gridToCoordsCenter(
          startPoint.gridX,
          startPoint.gridY
        );
        let enemy = { ...wave.enemy };
        enemy.x = startCoordinate.x;
        enemy.y = startCoordinate.y;
        enemyList.push(enemy);
        makeEnemyPath(enemy, towerList, roadList, endPoint);
        wave.lastDeployTime = gameTime;
      }
    }
  });
}

function logs() {
  //console.log(arguments);
}

function makeEnemyPath(enemy, towerList, roadList, endPoint) {
  let enemyGrid = coordsToGrid(enemy.x, enemy.y);
  enemy.path = callAStar(towerList, roadList, maxX, maxY, enemyGrid, endPoint);
}

function callAStar(towerList, roadList, maxX, maxY, startPoint, endPoint) {
  let stage = [];
  for (let x = 0; x < maxX; x++) {
    let row = [];
    for (let y = 0; y < maxY; y++) {
      row.push({
        x: x,
        y: y,
        f: Number.MAX_SAFE_INTEGER,
        g: Number.MAX_SAFE_INTEGER,
        free: true,
        road: false,
      });
    }
    stage.push(row);
  } // demolish
  towerList.forEach((tower) => {
    stage[tower.gridX][tower.gridY].free = false;
  });
  roadList.forEach((road) => {
    stage[road.gridX][road.gridY].road = true;
  });
  let aStar = aStarPathFinder(
    stage,
    stage[startPoint.gridX][startPoint.gridY],
    stage[endPoint.gridX][endPoint.gridY]
  );
  if (aStar === null) {
    return null;
  }
  let aStarVariable = aStar.map(function (c) {
    //logs("C: ", c);
    return { gridX: c.x, gridY: c.y };
  });
  logs(
    'aStarVariable.map: ',
    aStarVariable.map(function (c) {
      return c;
    })
  );
  return aStarVariable;
}

function moveEnemy(enemyList, elapsedTime) {
  enemyList.forEach((enemy) => {
    if (enemy.path.length > 0) {
      let currentStepGridCenter = gridToCoordsCenter(
        enemy.path[0].gridX,
        enemy.path[0].gridY
      );
      if (
        Math.abs(enemy.x - currentStepGridCenter.x) < 10 &&
        Math.abs(enemy.y - currentStepGridCenter.y) < 10
      ) {
        enemy.path.shift();

        calculateEnemyDirection(enemy);
      }
    }

    move(enemy, elapsedTime);
  });
}

function calculateEnemyDirection(enemy) {
  if (enemy.path.length === 0) {
    return;
  }
  let nextStepGrid = gridToCoordsCenter(
    enemy.path[0].gridX,
    enemy.path[0].gridY
  );
  /* logs(enemy.path[0]);
  logs(enemy.x, enemy.y);
  logs(coordsToGrid(enemy.x, enemy.y)); */
  enemy.direction =
    Math.atan2(nextStepGrid.y - enemy.y, nextStepGrid.x - enemy.x) +
    Math.PI / 2;
}

function moveBullet(bulletList, elapsedTime) {
  bulletList.forEach((bullet) => {
    move(bullet, elapsedTime);
  });
}

function bulletShoot(towerList, enemyList, elapsedTime, bulletList) {
  towerList.forEach((tower) => {
    tower.timeToReload -= elapsedTime;
    if (tower.timeToReload <= 0) {
      if (enemyList.length > 0) {
        let target = closestEnemy(enemyList, tower);
        if (target !== null) {
          shoot(target, bulletList, tower);
          tower.timeToReload = tower.reloadingTime;
        }
      }
    }
  });
}

function closestEnemy(enemyList, tower) {
  let actualTowerCoord = gridToCoordsCenter(tower.gridX, tower.gridY);
  let enemyOnFire = enemyList.filter((enemy) => {
    return (
      rangeDistance(actualTowerCoord, enemy) <
      Math.floor(tower.range * cellSize)
    );
  });
  if (enemyOnFire.length === 0) {
    return null;
  } else {
    logs(
      actualTowerCoord,
      enemyOnFire[0],
      rangeDistance(
        gridToCoordsCenter(tower.gridX, tower.gridY),
        enemyOnFire[0]
      )
    );
    return enemyOnFire[0];
  }
}
function rangeDistance(a, b) {
  let dX = Math.abs(a.x - b.x);
  let dY = Math.abs(a.y - b.y);
  let newDistance = Math.sqrt(dX * dX + dY * dY);
  return newDistance;
}

function shoot(target, bulletList, tower) {
  let bullet = {
    x: tower.gridX * cellSize + cellSize / 2,
    y: tower.gridY * cellSize + cellSize / 2,
    speed: 5,
    direction:
      Math.atan2(
        tower.gridY * cellSize - target.y,
        tower.gridX * cellSize - target.x
      ) -
      Math.PI / 2,
    size: cellSize / 2,
  };
  bulletList.push(bullet);
}

function move(object, elapsedTime) {
  let dx =
    object.speed * elapsedTime * Math.cos(object.direction - Math.PI / 2);
  let dy =
    object.speed * elapsedTime * Math.sin(object.direction - Math.PI / 2);

  object.x += dx;
  object.y += dy;
}

function coordsToGrid(x, y) {
  return { gridX: Math.floor(x / cellSize), gridY: Math.floor(y / cellSize) };
}

function gridToCoordsCenter(gridX, gridY) {
  return {
    x: gridX * cellSize + cellSize / 2,
    y: gridY * cellSize + cellSize / 2,
  };
}

function placeTower(gameState, point) {
  let { towerList, roadList, enemyList, startPoint, endPoint } = gameState;
  if (
    !isGridFree(
      [...towerList, ...roadList],
      enemyList,
      startPoint,
      endPoint,
      point
    )
  ) {
    return;
    //soundeffect
  }
  let tower = { ...gameState.tower, gridX: point.gridX, gridY: point.gridY };
  if (gameState.tower) {
    if (
      callAStar([...towerList, tower], [], maxX, maxY, startPoint, endPoint) ===
      null
    ) {
      return;
    }
    if (gameState.money >= 100) {
      towerList.push(tower);
      enemyList.forEach((enemy) => {
        makeEnemyPath(enemy, towerList, roadList, endPoint);
        calculateEnemyDirection(enemy);
      });

      gameState.money -= 40;
    }
  }
}

function isGridFree(towerList, enemyList, startPoint, endPoint, point) {
  if (compareGrids(point, startPoint) || compareGrids(point, endPoint)) {
    return false;
  }
  if (
    towerList.filter((tower) => {
      return compareGrids(tower, point);
    }).length > 0
  ) {
    return false;
  }
  if (
    enemyList.filter((enemy) => {
      return compareGrids(coordsToGrid(enemy.x, enemy.y), point);
    }).length > 0
  ) {
    return false;
  }
  return true;
}

function compareGrids(point1, point2) {
  return point1.gridX === point2.gridX && point1.gridY === point2.gridY;
}

function makeEnemy(x, y, speed, size, direction) {
  return {
    x,
    y,
    speed,
    size,
    direction,
  };
}

function addEnemy(numberOfEnemies) {
  for (let i = 0; i < numberOfEnemies; i++) {

  }
}

function drawSprite(img, sX, sY, sW, sH, dX, dY, dW, dH) {
  ctx.drawImage(img, sX, sY, sW, sH, dX, dY, dW, dH)
}




let t;
let gameSpeed = 1;
let timer_is_on = 1;

function gamePlayPause() {
  if (!timer_is_on) {
    clearTimeout(t);
    timer_is_on = 1;
  } else {
    clearTimeout(t);
    timer_is_on = 0;
  }
}

function makeGameSpeedSlower() {
  if (gameSpeed > 0.2) {
    gameSpeed = (gameSpeed * 10 - 1) / 10;
    gameSpeed.toFixed(1);
  }
}
function makeGameSpeedFaster() {
  if (gameSpeed < 5) {
    gameSpeed = (gameSpeed * 10 + 1) / 10;
    gameSpeed.toFixed(1);
  }
}




function frame(gameState, canvas, editor) {
  if (editor) {
    draw(gameState, canvas);
    setTimeout(() => frame(gameState, canvas, editor), 10);
    //requestAnimationFrame(frame(gameState, canvas, editor));

    return;
  }
  let gamerScore = JSON.parse(localStorage.getItem('HighScore') || '{}');
  if (
    !gamerScore[gameState.name] ||
    gamerScore[gameState.name] < gameState.score
  ) {
    gamerScore[gameState.name] = gameState.score;
    localStorage.setItem('HighScore', JSON.stringify(gamerScore));
  }
  let now = Date.now();
  let elapsedTime =
    ((now - gameState.lastUpdate) / 5) * timer_is_on * gameSpeed; // global speed helye
  for (let i = 0; i < elapsedTime - 1; ++i) {
    update(gameState, canvas, 1);
  }
  draw(gameState, canvas);
  setTimeout(() => frame(gameState, canvas, editor), 10);
  //requestAnimationFrame(frame(gameState, canvas, editor));
  gameState.lastUpdate = now;
}

function checkShoot(enemyList, bulletList) {
  function collision(enemy, bullet) {
    return (
      Math.abs(enemy.x - bullet.x) < enemy.size / 2 + bullet.size / 2 &&
      Math.abs(enemy.y - bullet.y) < enemy.size / 2 + bullet.size / 2
    );
  }

  let remainingBulletList = [];

  for (let j = 0; j < bulletList.length; ++j) {
    let hit = false;
    for (let i = 0; i < enemyList.length; ++i) {
      if (collision(enemyList[i], bulletList[j])) {
        enemyList[i].life -= 1;
        hit = true;
        break;
      }
    }
    if (!hit && remainingBulletList.indexOf(bulletList[j]) === -1) {
      remainingBulletList.push(bulletList[j]);
    }
  }
  let survivorList = enemyList.filter((enemy) => {
    return enemy.life > 0;
  });

  return { survivorList, remainingBulletList };
}
